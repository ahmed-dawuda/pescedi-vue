export default {
  chunks: (array, size) => {
    var results = []
    while (array.length) {
      results.push(array.splice(0, size))
    }
    return results
  },
  position (i) {
    var j = i % 10
    var k = i % 100
    if (j === 1 && k !== 11) {
      return 'st'
    }
    if (j === 2 && k !== 12) {
      return 'nd'
    }
    if (j === 3 && k !== 13) {
      return 'rd'
    }
    return 'th'
  }
}
