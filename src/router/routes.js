/* eslint-disable standard/object-curly-even-spacing,comma-dangle,object-property-newline */
import Login from './../components/auth-false/views/login.vue'
import Register from './../components/auth-false/views/register.vue'
import Fixtures from './../components/auth-true/views/pages/fixture/fixtures.vue'
import Create from './../components/auth-true/views/pages/league/create.vue'
import Slip from './../components/auth-true/views/pages/bet/slip.vue'
import History from './../components/auth-true/views/pages/slips/history.vue'
import About from './../components/auth-false/views/about.vue'
import MyTeam from './../components/auth-true/views/pages/team/myteam.vue'
import LeagueDetail from '../components/auth-true/views/pages/team/leagueDetail.vue'
import Pending from '../components/auth-true/views/pages/league/pending.vue'
import Completed from '../components/auth-true/views/pages/league/completed.vue'
import HowToPlay from '../components/auth-false/views/HowTo/howToPlay.vue'
import FAQ from '../components/auth-false/views/HowTo/faq.vue'
import Latest from '../components/auth-true/views/pages/league/latest.vue'
import Wallet from '../components/auth-true/views/pages/wallet.vue'
import AllTime from '../components/auth-true/views/pages/allTimeRanking.vue'

export default [
  { path: '/', name: 'main-home', redirect: '/welcome'},
  { path: '/welcome', name: 'welcome', component: About},
  { path: '/login', name: 'login', component: Login },
  { path: '/register', name: 'register', component: Register },
  { path: '/about', name: 'about', component: About},
  { path: '/fixtures', name: 'fixtures', component: Fixtures},
  { path: '/league/create', name: 'create', component: Create},
  { path: '/slip', name: 'slip', component: Slip},
  { path: '/history', name: 'history', component: History},
  { path: '/myteam', name: 'myteam', component: MyTeam},
  { path: '/league/:id/:comp', name: 'league-detail', component: LeagueDetail},
  { path: '/leagues/pending', name: 'pending-leagues', component: Pending},
  { path: '/leagues/completed', name: 'completed-leagues', component: Completed},
  { path: '/howTo/play', name: 'howToPlay', component: HowToPlay},
  { path: '/howTo/faq', name: 'faq', component: FAQ},
  { path: '/leagues/latest', name: 'latest-leagues', component: Latest},
  { path: '/wallet', name: 'wallet', component: Wallet},
  { path: '/alltimeranking', name: 'alltimeranking', component: AllTime}
]
