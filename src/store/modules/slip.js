/* eslint-disable no-trailing-spaces */
const state = {
  slip: [],
  won: false,
  cleared: true,
  start: false,
  done: false,
  deactivate: false,
  simError: false
}

const getters = {
  simError (state) {
    return state.simError
  },
  slip (state) {
    return state.slip
  },
  sliplen (state) {
    return state.slip.length
  },
  finalSlip (state) {
    let slip = state.slip
    let bet = []
    slip.forEach(item => {
      bet.push({id: item.id, home: item.home.id, away: item.away.id, bet: item.bet})
    })
    return bet
  },
  odd (state) {
    let odds = 1
    state.slip.forEach(item => {
      odds *= item.bet.odd
    })
    return odds
  },
  start (state) {
    return state.start
  },
  won (state) {
    return state.won
  },
  done (state) {
    return state.done
  },
  cleared (state) {
    return state.cleared
  },
  deactivate (state) {
    return state.deactivate
  }
}

const mutations = {
  'ADD_TO_SLIP' (state, payload) {
    if (state.done === true && state.cleared === false) {
      state.slip = []
      state.done = false
      state.cleared = true
      state.deactivate = true
    }
    let found = false
    for (let i = 0; i < state.slip.length; i++) {
      if (state.slip[i].id === payload.id) {
        found = true
      }
    }
    if (found === false) {
      state.slip.push(payload)
    }
    state.deactivate = false
  },
  'REMOVE_FROM_SLIP' (state, payload) {
    for (var i = 0; i < state.slip.length; i++) {
      if (state.slip[i].id === payload.id) {
        if (payload.bet.type === state.slip[i].bet.type) {
          state.slip.splice(i, 1)
        }
      }
    }
  },
  'REMOVE_SELECTED' (state, id) {
    for (var i = 0; i < state.slip.length; i++) {
      if (state.slip[i].id === id) {
        state.slip.splice(i, 1)
      }
    }
  },
  'SIMULATE_SLIP' (state, payload) {
    let slip = state.slip
    for (var i = 0; i < payload.length - 1; i++) {
      const bet = slip.find(element => {
        return element.id === payload[i].id
      })
      bet.score = payload[i].score
      bet.status = payload[i].won
    }
    state.won = payload[payload.length - 1].won
    state.start = true
  },
  'DONE' (state) {
    state.done = true
    state.start = false
    state.cleared = false
  },
  'DEACTIVATE' (state, payload) {
    state.deactivate = payload
  },
  'SIM_ERROR' (state, payload) {
    state.simError = payload
  },
  'CLEAR_SLIP' (state) {
    state.slip = []
  }
}

const actions = {
  add_to_slip ({commit}, payload) {
    commit('ADD_TO_SLIP', payload)
  },
  remove_from_slip ({commit}, payload) {
    commit('REMOVE_FROM_SLIP', payload)
  },
  simulate ({commit}, payload) {
    commit('SIMULATE_SLIP', payload)
  },
  done ({commit}) {
    commit('DONE')
  },
  remove_selected ({commit}, id) {
    commit('REMOVE_SELECTED', id)
  },
  deactivate ({commit}, payload) {
    commit('DEACTIVATE', payload)
  },
  simError ({commit}, payload) {
    commit('SIM_ERROR', payload)
  },
  clear_slip ({commit}) {
    commit('CLEAR_SLIP')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
