const state = {
  history: null
}

const getters = {
  history (state) {
    return state.history
  },
  historyLen (state) {
    return state.history.length
  }
}

const mutations = {
  'ADD_TO_HISTORY' (state, payload) {
    state.history.push(payload)
  },
  'SET_HISTORY' (state, payload) {
    state.history = payload
  },
  'CLEAR_HISTORY' (state) {
    state.history = []
  }
}

const actions = {
  add_to_history ({commit}, payload) {
    commit('ADD_TO_HISTORY', payload)
  },
  set_history ({commit}, payload) {
    commit('SET_HISTORY', payload)
  },
  clear_history ({commit}) {
    commit('CLEAR_HISTORY')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
