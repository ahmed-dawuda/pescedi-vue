const state = {
  pendingLeagues: 0,
  members: 0,
  leaguesWon: 0,
  betwins: 0,
  maxwins: 0
}

const getters = {
  members (state) {
    return state.members
  },
  pendingLeagues (state) {
    return state.pendingLeagues
  },
  leaguesWon (state) {
    return state.leaguesWon
  },
  betwins (state) {
    return state.betwins
  },
  maxwins (state) {
    return state.maxwins
  }
}

const mutations = {
  'UPDATE_HOME' (state, payload) {
    state.pendingLeagues = payload.pendingLeagues
    state.members = payload.members
    state.leaguesWon = payload.leaguesWon
    state.betwins = payload.betwins
    state.maxwins = payload.maxwins
    // console.log(state)
  }
}

const actions = {
  update_home ({commit}, payload) {
    commit('UPDATE_HOME', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
