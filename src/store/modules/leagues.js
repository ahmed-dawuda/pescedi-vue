const state = {
  completed: null,
  pending: null,
  teams: []
}

const getters = {
  league_teams (state) {
    return state.teams
  },
  completed (state) {
    return state.completed
  },
  pending (state) {
    return state.pending
  },
  findCompleted (state) {
    return (id) => {
      let league = null
      if (state.completed !== null) {
        for (let i = 0; i < state.completed.length; i++) {
          if (state.completed[i].id === id) {
            league = state.completed[i]
            break
          }
        }
      } else {
        return {}
      }
      return league
    }
  },
  findPending (state) {
    return (id) => {
      let league = null
      if (state.pending !== null) {
        for (let i = 0; i < state.pending.length; i++) {
          if (state.pending[i].id === id) {
            league = state.pending[i]
            break
          }
        }
      } else {
        return {}
      }
      return league
    }
  }
}

const mutations = {
  'SET_COMPLETED' (state, payload) {
    state.completed = payload
  },
  'SET_PENDING' (state, payload) {
    state.pending = payload
  },
  'DELETE_PENDING' (state, id) {
    for (let i = 0; i < state.pending.length; i++) {
      if (state.pending[i].id === id) {
        state.pending.splice(i, 1)
        break
      }
    }
  },
  'ADD_TEAM' (state, teams) {
    state.teams = teams
  }
}

const actions = {
  set_completed ({commit}, payload) {
    commit('SET_COMPLETED', payload)
  },
  set_pending ({commit}, payload) {
    commit('SET_PENDING', payload)
  },
  delete_pending ({commit}, id) {
    commit('DELETE_PENDING', id)
  },
  add_team ({commit}, teams) {
    commit('ADD_TEAM', teams)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
