const state = {
  fixtures: [],
  fix: []
}

const getters = {
  fixtures (state) {
    return state.fixtures
  },
  no_fixtures (state) {
    return state.fixtures.length
  },
  fix (state) {
    return state.fix
  }
}

const mutations = {
  'SET_FIXTURES' (state, payload) {
    state.fixtures = payload[0]
    state.fix = payload[1]
  },
  'CLEAR_FIXTURES' (state) {
    state.fixtures = []
  }
}

const actions = {
  set_fixtures ({commit}, payload) {
    commit('SET_FIXTURES', payload)
  },
  clear_fixtures ({commit}) {
    commit('CLEAR_FIXTURES')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
