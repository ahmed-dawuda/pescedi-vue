/* eslint-disable no-trailing-spaces,space-before-blocks,no-unused-vars,space-before-function-paren */

const state = {
  user: null,
  loginError: false
}

const getters = {
  loginError (state) {
    return state.loginError
  },
  hasTeam (state) {
    return state.user.team
  },

  auth (state) {
    return state.user !== null
  },
  user (state) {
    return state.user
  },
  token (state) {
    if (state.user !== null) {
      return state.user.api_token
    } else {
      return 'Bearer'
    }
  },
  config(state){
    return {
      'Authorization': 'Bearer ' + state.user.api_token,
      'Content-Type': 'Application/json',
      'X-Requested-With': 'XMLHttpRequest'
    }
  }
}

const mutations = {
  'SET_USER' (state, payload) {
    state.user = payload
  },
  'CLEAR_USER' (state) {
    state.user = null
  },
  'SET_LOGIN_ERROR' (state, payload) {
    state.loginError = payload
  }
}

const actions = {

  login ({commit}, payload) {
    commit('SET_USER', payload.user)
    localStorage.setItem('remember', JSON.stringify(payload.remember))
    localStorage.setItem('user', JSON.stringify(payload.user))
    sessionStorage.setItem('user', JSON.stringify(payload.user))
    localStorage.setItem('name', JSON.stringify(payload.user.username))
    return true
  },
  localLogin ({commit}, payload) {
    commit('SET_USER', payload)
  },
  logout ({commit}) {
    commit('CLEAR_USER')
    localStorage.removeItem('user')
    sessionStorage.removeItem('user')
  },
  loginError ({commit}, payload) {
    commit('SET_LOGIN_ERROR', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
