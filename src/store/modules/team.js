const state = {
  team: null,
  profile: '',
  trophies: 0,
  rank: 0,
  bot: false
}

const getters = {
  bot (state) {
    return state.bot
  },
  rank (state) {
    return state.rank
  },
  trophies (state) {
    return state.trophies
  },
  team_heading (state) {
    return {
      trophies: state.trophies,
      rank: state.rank
    }
  },
  profile (state) {
    return state.profile
  },
  strategy (state) {
    return {
      points: state.team.points,
      attack: state.team.attack,
      midfield: state.team.midfield,
      defense: state.team.defense,
      name: state.team.name
    }
  },
  team (state) {
    return state.team
  }
}

const mutations = {
  'SET_TEAM' (state, payload) {
    state.team = payload.team
    state.profile = payload.profile
    state.trophies = payload.trophies
    state.rank = payload.rank
    state.bot = payload.bot
  },

  'UPDATE_STRATEGY' (state, payload) {
    if (state.team !== null) {
      state.team.points = payload.points
      state.team.attack = payload.attack
      state.team.midfield = payload.midfield
      state.defense = payload.defense
    }
  },
  'UPLOAD_PROFILE' (state, payload) {
    state.profile = payload
  },
  'CHANGE_BOT' (state, payload) {
    state.bot = payload
  }
}

const actions = {
  upload_profile ({commit}, payload) {
    commit('UPLOAD_PROFILE', payload)
  },
  update_strategies ({commit}, payload) {
    commit('UPDATE_STRATEGY', payload)
  },
  set_team ({commit}, payload) {
    commit('SET_TEAM', payload)
  },
  change_bot ({commit}, payload) {
    commit('CHANGE_BOT', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
