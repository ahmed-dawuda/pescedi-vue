/* eslint-disable eol-last,semi */

import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import fixtures from './modules/fixtures'
import slip from './modules/slip'
import history from './modules/sliphistory'
import home from './modules/home'
import leagues from './modules/leagues'
import team from './modules/team'

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    user,
    fixtures,
    slip,
    history,
    home,
    leagues,
    team
  }
})
