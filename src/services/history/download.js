/* eslint-disable no-trailing-spaces,indent */
import Vue from 'vue'
import { store } from '../../store/index'
import Axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, Axios)

export default {
  download () {
    Axios({
      url: 'user/slips',
      method: 'get',
      headers: store.getters.config
    }).then(response => {
      // console.log(response.data.data)
      store.dispatch('set_history', response.data.data)
    }).catch(error => {
      console.log(error.response.data)
    })
  }
}
