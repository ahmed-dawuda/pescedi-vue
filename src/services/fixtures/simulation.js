/* eslint-disable no-trailing-spaces,indent */
import Vue from 'vue'
import { store } from '../../store/index'
import Axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, Axios)

export default (stake) => {
  Axios({
    url: 'simulate',
    method: 'post',
    data: {slip: store.getters.finalSlip, stake: stake},
    headers: store.getters.config
  }).then(response => {
    // console.log(response.data.data)
    store.dispatch('simulate', response.data.data)
  }).catch(() => {
    store.dispatch('simError', true)
    // console.log(error.response.data)
  })
}
