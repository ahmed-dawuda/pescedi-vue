/* eslint-disable no-trailing-spaces,indent,camelcase */
import Vue from 'vue'
import { store } from '../../store/index'
import Axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, Axios)

export default (bet) => {
  // console.log('service: ', bet)
  let fixtures = store.getters.fix
  fixtures.forEach(item => {
    if (item.id === bet.id) {
      // console.log(item.id, bet.id)
      let slip_item = {id: item.id, home: item.home, away: item.away, bet: {type: bet.type, odd: bet.odd}}
      store.dispatch('add_to_slip', slip_item)
    }
  })
}
