/* eslint-disable no-trailing-spaces,indent */
import Vue from 'vue'
import { store } from '../../store/index'
import Axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, Axios)

export default {
  download () {
    Axios.get('fixtures')
      .then(response => {
        // console.log(response.data.data)
        store.dispatch('set_fixtures', [response.data.data, response.data.fix])
      })
      .catch((error) => { console.log(error.response.data) })
  }
}
