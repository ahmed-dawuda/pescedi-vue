/* eslint-disable no-trailing-spaces,indent */
import Vue from 'vue'
import { store } from '../../store/index'
import Axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, Axios)

export default {
  myTeam () {
    Axios({
      url: 'user/team',
      method: 'get',
      headers: store.getters.config
    }).then(response => {
      // console.log(response.data.data)
      store.dispatch('set_team', response.data.data)
    }).catch(error => {
      console.log(error.response.data)
    })
  },
  leagues () {
    Axios({
      url: 'user/leagues',
      method: 'get',
      headers: store.getters.config
    }).then(response => {
      // console.log(response.data.data)
      store.dispatch('set_completed', response.data.data.completed)
      store.dispatch('set_pending', response.data.data.pending)
    }).catch(error => {
      console.log(error.response.data)
    })
  }
}
