/* eslint-disable no-trailing-spaces,indent */
import Vue from 'vue'
import { store } from '../../store/index'
import Axios from 'axios'
import VueAxios from 'vue-axios'
import OtherDownloads from '../others/downloads'
import FixtureDownload from '../fixtures/download'
// import HistoryDownload from '../history/download'

Vue.use(VueAxios, Axios)

export default {

  login (credentials, remember) {
    const thisService = this
     Axios({
      url: 'login',
      method: 'post',
      data: credentials
    }).then(response => {
      store.dispatch('login', {user: response.data.data, remember})
       thisService.downloads()
    }).catch(() => {
      // console.log(error.response.data)
       store.dispatch('loginError', true)
    })
  },
  localLogin () {
    let loggedin = false
    const remember = JSON.parse(localStorage.getItem('remember'))
    if (remember === true) {
      const luser = JSON.parse(localStorage.getItem('user'))
      if (luser !== null) {
        store.dispatch('localLogin', luser)
        loggedin = true
      }
    } else {
      const suser = JSON.parse(sessionStorage.getItem('user'))
      if (suser !== null) {
        store.dispatch('localLogin', suser)
        loggedin = true
      }
    }
    if (loggedin) {
      this.downloads()
    }
    return loggedin
  },
  downloads () {
    // OtherDownloads.leagues()
    FixtureDownload.download()
    // HistoryDownload.download()
    if (store.getters.auth) {
      if (store.getters.hasTeam) {
        OtherDownloads.myTeam()
      }
    }
  }

}
