import Vue from 'vue'
// import { store } from '../../store/index'
import Axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, Axios)

export default {
  register (formData) {
    Axios({
      url: '/register',
      method: 'post',
      data: formData
    }).then(response => {
      console.log(response.data.data)
    }).catch(error => {
      console.log(error.response.data)
    })
  }
}
