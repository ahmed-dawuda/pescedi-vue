/* eslint-disable indent */
import {store} from '../../store/index'

export default () => {
    store.dispatch('logout')
    localStorage.setItem('remember', JSON.stringify(false))
    localStorage.removeItem('user')
    sessionStorage.removeItem('user')
    store.dispatch('clear_slip')
    store.dispatch('clear_history')
    store.dispatch('clear_fixtures')
}
