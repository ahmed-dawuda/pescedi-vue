/* eslint-disable quotes,no-undef */
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import {router} from './router'
import Axios from 'axios'
import VueAxios from 'vue-axios'
import { store } from './store/index'
import VeeValidate from 'vee-validate'

Vue.use(VueAxios, Axios)
Vue.use(VeeValidate)
Vue.axios.defaults.baseURL = "https://api.pescedi.com/api/v1/"

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
